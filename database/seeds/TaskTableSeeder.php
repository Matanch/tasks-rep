<?php

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert(
        [
            [
                'title' => 'go to shopping',
                'status' => 'undone',
                'user_id'=> 1 , 
                'created_at' =>date('Y-m-d G:i:s'),
                'updated_at'=>date('Y-m-d G:i:s'),
            ],
            [
                'title' => 'go bay sun glass',
                'status' => 'done',
                'user_id'=> 2 , 
                'created_at' =>date('Y-m-d G:i:s'),
                'updated_at'=>date('Y-m-d G:i:s'),
            ],
            [
                'title' => 'make a nod on eliran srur',
                'status' => 'undone',
                'user_id'=> 1 , 
                'created_at' =>date('Y-m-d G:i:s'),
                'updated_at'=>date('Y-m-d G:i:s'),
            ]
        ]

    );
    }
}