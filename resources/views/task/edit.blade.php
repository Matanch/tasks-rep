@extends('layouts.app')
@section('content')

<head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        </head>
     <!--Update-->

        <div class="container">
            <br/><br/>
            <h3>Edit Task</h3>
            
            <form method="post" action ="{{action('TaskController@update' , $task->id)}}">
                {{csrf_field()}}
                @method('PATCH')
                    <div class="form-group">
                        <label for="title"> Update task</label>
                        <input type ="text" class ="form-control" name="title" value = "{{$task->title}}">
                    </div>
                   
                    <br>
                    <div class ="container">
                        <div class="col-4  offset-4">
                            <input type ="submit" class="form-control btn btn-secondary" name="submit" value =" save "> 
                        </div>
                    </div>

                    <br>
                    <div class ="container">
                        <div class="col-4  offset-4">
                            <a href="{{route('task.index')}}" class=" form-control btn btn-secondary">Back to list</a>
                         </div>
                    </div>
            </form>
        </div>

       
        
        @endsection