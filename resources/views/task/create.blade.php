@extends('layouts.app')
@section('content')

<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<div class="container">
    <br/><br/>
    <h3> Add new task</h3>
        
    <form method="post" action ="{{action('TaskController@store')}}">
            {{csrf_field()}}
            <div class="form-group">
                <label for="title">right your new task</label>
                <input type ="text" class ="form-control" name="title">
            </div>

                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>   
                
            <div class ="container">
                <div class="col-4  offset-4">
                     <input type ="submit" class="form-control btn btn-secondary" name="submit" value ="Create"> 
                </div>
            </div> 
        <br><br>
        <div class ="container">
            <div class="col-4  offset-4">
                <a href="{{route('task.index')}}" class=" form-control btn btn-secondary">Back to list</a>
            </div>
        </div>
  </div>

    </form>
  

</div>

@endsection

