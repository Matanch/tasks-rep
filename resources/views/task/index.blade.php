@extends('layouts.app')
@section('content')

<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<div class='container'>
<H1>All tasks</H1>

  @isset($filtered)
     <a href = {{route('task.index')}}>All Tasks</a>
    @else
     <a href = {{route('myfilter')}}>My tasks</a>
    @endisset

    <br/><br/>
      <table class="table table-bordered">

      
          <thead class="thead-dark">
            <tr>
              <th scope="col">id</th>
              <th scope="col">Task title</th>
              <th scope="col">user_id</th>
              <th scope="col">Status</th>
              <th scope="col">Created at</th>
              <th scope="col">Updated at</th>
              <th scope="col">Edit </th>
              @can('admin')
              <th scope="col">Delete </th>
              @endcan


            </tr>
          </thead>
          <tbody>
          @foreach($tasks as $task)
         
            <tr>
                <td>{{$task->id}}</td>
                <td>{{$task->title}}</td>
                <td>{{$task->user_id}}</td>
                <td>
                @if($task->status == 0)
                @can('admin')
              
                <a href = "{{route('done' , $task->id)}}">Mark as done</a>
                @endcan
                @else
                Done!
                @endif
              
                </td>
                
                <td>{{$task->created_at}}</td> 
                <td>{{$task->updated_at}}</td>
                <td><a href = "{{route('task.edit' , $task->id)}}">Edit</td>
                @method('DELETE')
                @can('admin')
                <td><a href = "{{route('delete', $task->id)}}" class=" btn btn-secondary">Delete</td></td>  
                @endcan
            </tr>
            @endforeach
          </tbody>
      </table>

      <div class ="container">
        <div class="col-0  offset-0">
           <a href="{{route('task.create')}}" class=" btn btn-secondary">create a new task</a>
       </div>
      @endsection