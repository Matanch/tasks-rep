<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Collection;
use App\Task;
use App\User;
class TaskController extends Controller

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all();
        return view('task.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('task.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'price' => 'required',

        ]);

        $task = new Task();
        $id = Auth::id(); //the idea of the current user
        $task->title = $request->title;
        $task->user_id = $id;
        $task->status = 0;
        $task->price =  $request->price;
        $task->save();
        return redirect('task');         //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::findOrFail($id);
        return view('task.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //only if this todo belongs to user 
         $task = Task::findOrFail($id);            
         $task->update($request->except(['_token'])); 
         return redirect('task');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to create todos..");
        }
 
        $task = Task::find($id);
        $task->delete();
        return redirect('task');

    }
    public function done($id)
    {
        //only if this todo belongs to user 
        if (Gate::denies('admin')) {
            abort(403,"You are not allowed to mark tasks as done..");
         }          
        $task = Task::findOrFail($id);            
        $task->status = 1; 
        $task->save();
        return redirect('task');    
    }    

    public function indexFiltered()
    {
        $id = Auth::id(); //the current user 
        $tasks = Task::where('user_id', $id)->get();
        $filtered = 1; //this is to mark the view to show link to all tasks 
        return view('task.index', compact('tasks','filtered'));
    }    

    public function sort()
    {
        $sorted = Task::All();
        $tasks = $sorted->sortBy('price');
        return view('task.index', compact('tasks'));
    }    
}
